<?php
namespace Deployer;

require 'recipe/common.php';

// ------------- configuration -------------

set('allow_anonymous_stats', false);

set('application', function() {
	return getenv('CI_PROJECT_NAME');
});

set('user', function(){
	return getenv('GITLAB_USER_NAME');
});

set('repository', function() {
	return getenv('CI_REPOSITORY_URL');
});

set('deploy_path', function() {
	$basePath = getenv('DEPLOYER_BASE_PATH');
	$projectName = getenv('CI_PROJECT_NAME');
	$environment = get('stage');

	return "$basePath/$projectName/$environment";
});

set('shared_files', ['config/local.neon', '.env.local']);
set('shared_dirs', ['log']);
set('writable_dirs', ['log', 'temp']);
set('clear_paths', ['temp/cache', 'temp/proxies']);
set('copy_dirs', ['vendor']);
set('keep_releases', 2);

// ------------- environments -------------

host('production')
	->stage('production')
	->hostname(getenv('DEPLOYER_HOST_PRODUCTION'))
	->user('deployer')
	->addSshOption('StrictHostKeyChecking', 'no');

// ------------- tasks -------------

task('deploy:db', 'bin/console migrations:migrate --no-interaction --allow-no-migration');

task('deploy:composer', 'composer install -o --no-dev');

task('deploy:vue', function() {
	upload('dist/', '{{release_path}}/dist');
});

task('deploy', [
	'deploy:info',
	'deploy:prepare',
	'deploy:lock',
	'deploy:release',
	'deploy:update_code',
	'deploy:shared',
	'deploy:writable',
	'deploy:copy_dirs',
	'deploy:composer',
	'deploy:vue',
	'deploy:clear_paths',
	'deploy:db',
	'deploy:symlink',
	'deploy:unlock',
	'cleanup',
	'success'
]);

after('deploy:failed', 'deploy:unlock');
