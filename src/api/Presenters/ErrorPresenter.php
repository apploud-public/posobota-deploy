<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Application\Response;
use Nette\Application\Responses\JsonResponse;
use Nette\Application\UI\Presenter;
use Nette\SmartObject;
use Tracy\ILogger;

final class ErrorPresenter extends Presenter
{
	use SmartObject;

	/** @var ILogger */
	private $logger;


	public function __construct(ILogger $logger)
	{
		parent::__construct();
		$this->logger = $logger;
	}


	public function run(Nette\Application\Request $request): Response
	{
		$exception = $request->getParameter('exception');

		if ($exception instanceof Nette\Application\BadRequestException === false) {
			$this->logger->log($exception, ILogger::EXCEPTION);
		}

		return new JsonResponse([
			'error' => $exception->getCode(),
		]);
	}
}
