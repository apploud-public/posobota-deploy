<?php

declare(strict_types=1);

namespace Migrations;

use App\Counter\Counter;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210422225622 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
	    $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    $this->addSql("INSERT INTO counter (name, value) VALUES ('" . Counter::NAME_PAGE_LOADS . "', 0)");
    }

    public function down(Schema $schema) : void
    {
    }
}
