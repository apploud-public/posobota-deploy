<?php

declare(strict_types=1);

namespace Migrations;

use App\Counter\Counter;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210423120740 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
	    $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    $this->addSql("INSERT INTO counter (name, value) VALUES ('" . Counter::NAME_ANOTHER . "', 42)");
    }

    public function down(Schema $schema) : void
    {
    }
}
