<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Counter\CounterFacade;
use Nette;

final class ApiPresenter extends Nette\Application\UI\Presenter
{
	/** @var CounterFacade */
	private $counterFacade;


	public function __construct(CounterFacade $counterFacade)
	{
		parent::__construct();
		$this->counterFacade = $counterFacade;
	}


	public function actionDefault(): void
	{
		$this->sendJson(['alive' => true]);
	}

	public function actionPageLoads(): void
	{
		$this->sendJson(['count' => $this->counterFacade->pageLoad()]);
	}

	public function actionCounter(): void
	{
		$this->sendJson(['count' => $this->counterFacade->another()]);
	}
}
