<?php

declare(strict_types = 1);

namespace App\Counter;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Counter
{
	public const NAME_PAGE_LOADS = 'pageLoads';
	public const NAME_ANOTHER = 'another';

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(type="string", unique=true)
	 */
	private $name;

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 */
	private $value;


	public function getValue(): int
	{
		return $this->value;
	}


	public function increase(): void
	{
		$this->value++;
	}
}
