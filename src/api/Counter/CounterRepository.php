<?php

declare(strict_types = 1);

namespace App\Counter;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use RuntimeException;

class CounterRepository
{
	/** @var ObjectRepository<Counter> */
	private $doctrineRepository;


	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->doctrineRepository = $entityManager->getRepository(Counter::class);
	}


	public function getByName(string $name): Counter
	{
		/** @var Counter|null $counter */
		$counter = $this->doctrineRepository->findOneBy(['name' => $name]);

		if ($counter === null) {
			throw new RuntimeException('Counter with given name does not exists');
		}

		return $counter;
	}
}
