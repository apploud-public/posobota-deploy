<?php

declare(strict_types = 1);

namespace App\Counter;

use Doctrine\ORM\EntityManagerInterface;

class CounterFacade
{
	/** @var CounterRepository */
	private $repository;

	/** @var EntityManagerInterface */
	private $entityManager;


	public function __construct(CounterRepository $repository, EntityManagerInterface $entityManager)
	{
		$this->repository = $repository;
		$this->entityManager = $entityManager;
	}


	public function pageLoad(): int
	{
		$counter = $this->repository->getByName(Counter::NAME_PAGE_LOADS);
		$counter->increase();
		$this->entityManager->flush();
		return $counter->getValue();
	}


	public function another(): int
	{
		$counter = $this->repository->getByName(Counter::NAME_ANOTHER);
		$counter->increase();
		$this->entityManager->flush();
		return $counter->getValue();
	}
}
